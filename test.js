var mongo = require('mongodb'),
  Server = mongo.Server,
  Db = mongo.Db;

var server = new Server('localhost', 27017, {auto_reconnect: true});
var db = new Db('exampleDb', server);

db.open(function(err, db) {
  if(!err) {
    console.log("Connected!");
  }



	db.collection('namesCollection', function(err, collectionref) { 
    	var myDocs = [{"name":"Mary1"}, {"name":"Mary2"}, {"name":"Mary3"}];

    	// добавляем имена
		collectionref.insert(myDocs, function (err, result) {
    		if(!err)  {
    			console.log('Names added!');

    			// меняем первое имя на другое
    			collectionref.update({name:"Mary1"}, {name:"Anny1"}, {multi:"true"}, function(err) {

    				// удаляем второе имя
    				collectionref.remove({name:"Mary2"}, function(err) {

    					// выводим то, что получилось
    					var cursor = collectionref.find();
						cursor.toArray(function(err, docs) {
    						console.log(docs);
    						collectionref.drop('namesCollection', function(err){
    							db.close();
    						});
						});
    				})
    			});

    		} else console.log('Names not added');
		});
	});

});