var express = require("../server-express/node_modules/express");
var app     = express();

// var mongo = require("mongodb"),
//   Server = mongo.Server,
//   Db = mongo.Db;

var mongoose    = require('mongoose');
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/tasks');
var db = mongoose.connection;

var Schema = mongoose.Schema;
var taskSchema = new Schema({
    name: { type: String, required: true },
    owner: { type: String, required: true },
    description: { type: String, required: true },
    modified: { type: Date, default: Date.now },
    done: { type: Boolean, default: false }
});

var TaskModel = mongoose.model('Task', taskSchema);

var userSchema = new Schema({
    name: { type: String, required: true },
});

var UserModel = mongoose.model('User', userSchema);

db.on('error', console.error.bind(console, "connection error: "));
db.once('open', function callback () {
    console.log("Connected to DB!");

    // ВЫВОД СПИСКА ПОЛЬЗОВАТЕЛЕЙ
    app.use("/api/users/get", (req, res) => {
        UserModel.find({}, function(err, docs) {
            if (!err) {
                var users = "";
                for (var i = 0; i < docs.length; i++) {
                    users +=  i + 1 + ". " + docs[i].name + "<br>";
                }
            res.writeHead(200, { "Content-Type": "text/html" });
            res.write(users);
            } else {
                 res.status(400).send(err);
            }
        });
    });

    // ДОБАВЛЕНИЕ ПОЛЬЗОВАТЕЛЯ
    app.use("/api/users/add", (req, res) => {
        var username = req.path.replace("/", "");
        if (username !== "") {
            var user = new UserModel({ name:  username});
            user.save(function (err) {
            if (!err) {
                res.status(200).send("User " + username + " added!");
            } else {
                 res.status(400).send(err);
            }
        });

        } else {
            res.status(400).send("Enter user name");
        }
    });

    // РЕДАКТИРОВАНИЕ ПОЛЬЗОВАТЕЛЯ ПО ИМЕНИ
    app.use("/api/users/update", (req, res) => {
        if (req.query.name !== "" && req.query.name) {
            var user = new UserModel();
            user.update({ name: req.query.name }, { name: req.query.newname}, function (err) {
                if (!err) {
                    res.status(200).send("User " + req.query.name + " updated to new name " + req.query.newname + "!");
                } else {
                    res.status(400).send(err);
                }
            });
        } else {
            res.status(400).send("Enter user name");
        }
    });

    // УДАЛЕНИЕ ПОЛЬЗОВАТЕЛЯ
    app.use("/api/users/delete", (req, res) => {
        if (req.query.name !== "") {
            var user = new UserModel();
            var query = JSON.stringify({ name: req.query.name });
            console.log(query);
            user.remove(query, function (err) {
            if (!err) {
                res.status(200).send("User " + req.query.name + " removed!");
            } else {
                 res.status(400).send(err);
            }
        });

        } else {
            res.status(400).send("Enter user name");
        }
    });

    // TaskModel.update({ owner: "Owner 3"}, { owner: "User 3"}, function(req, res) {
    //     console.log(res);
    // });
    

    // TaskModel.find({}, function(err, docs) {
    //     console.log(docs);
    // });

    // UserModel.find({}, function(err, docs) {
    //     console.log(docs);
    // });

    app.use((req, res, next) => {
        res.status(404).send("Path not found");
    });

    app.use((err, req, res, next) => {
      res.status(500);
      res.render("error", { error: err });
      next(err);
    });

    app.listen(3000);


});

// var server = new Server("localhost", 27017, {auto_reconnect: true});
// var db = new Db("exampleDb", server);

// // ВЫВОД СПИСКА
// app.use("/api/contacts/get", (req, res) => {
//     db.open(function(err, db) {
//         db.collection("phoneBook", function(err, collectionref) { 
//             var cursor = collectionref.find();
//             cursor.toArray(function(err, docs) {
//                 var contacts = "";
//                 for (var i = 0; i < docs.length; i++) {
//                     contacts += docs[i].name + " " + docs[i].surname + ", " + docs[i].phone + "<br>";
//                 }
//                 res.writeHead(200, { "Content-Type": "text/html" });
//                 res.write(contacts);
//                 db.close();
//             });
//         });
//     });
// });

// // ДОБАВЛЕНИЕ КОНТАКТА
// app.use("/api/contacts/add", (req, res) => {
//     if ((!req.query.name) || (!req.query.surname) || (!req.query.phone)) {
//         res.status(400).send("Enter name, surname and phone");
//     } else {
//         db.open(function(err, db) {
//             db.collection("phoneBook", function(err, collectionref) { 
//                 var newContact = {"name": req.query.name, "surname": req.query.surname, "phone": req.query.phone};
//                 collectionref.insert(newContact, function (err, result) {
//                     if(!err)  {
//                         res.status(200).send("Contact " + req.query.name + " " + req.query.surname + " added");
//                         db.close();
//                     }
//                 });
//             });
//         });
//     }
// });

// // ИЗМЕНЕНИЕ НОМЕРА У КОНТАКТА
// app.use("/api/contacts/newphone", (req, res) => {
//     if ((!req.query.name) || (!req.query.surname) || (!req.query.newphone)) {
//         res.status(400).send("Enter name, surname and new phone number");
//     } else {
//         db.open(function(err, db) {
//             db.collection("phoneBook", function(err, collectionref) { 
//                 collectionref.update(   { name: req.query.name, surname: req.query.surname },
//                                         { name: req.query.name, surname: req.query.surname, phone: req.query.newphone },
//                                         { upsert: true }, function (err, result) {
//                     if(!err)  {
//                         res.status(200).send("Contact " + req.query.name + " " + req.query.surname + " updated");
//                         db.close();
//                     }
//                 });
//             });
//         });
//     }
// });

// //УДАЛЕНИЕ КОНТАКТА
// app.use("/api/contacts/remove", (req, res) => {
//     if ((!req.query.name) || (!req.query.surname)) {
//         res.status(400).send("Enter name and surname");
//     } else {
//         db.open(function(err, db) {
//             db.collection("phoneBook", function(err, collectionref) { 
//                 collectionref.remove({ name: req.query.name, surname: req.query.surname }, function (err, result) {
//                     if(!err)  {
//                         res.status(200).send("Contact " + req.query.name + " " + req.query.surname + " removed");
//                         db.close();
//                     }
//                 });
//             });
//         });
//     }
// });


// // ПОИСК ПО ИМЕНИ, ФАМИЛИИ, ТЕЛЕФОНУ
// app.use("/api/contacts/find/", (req, res) => {
//     db.open(function(err, db) {
//         db.collection("phoneBook", function(err, collectionref) { 
//             if((req.path !== "/name") && (req.path !== "/surname") && (req.path !== "/phone")) {
//                 res.status(400).send("Search path is incorrect");
//             } else {
//                 var keyword = Object.keys(req.query)[0];
//                 var keyname = req.path.replace("/", ""); // не смог преобразовать keyname для запроса!! :((
//                 switch(req.path) {
//                     case "/name"    :       cursor = collectionref.find({ name: keyword });
//                     break;
//                     case "/surname" :       cursor = collectionref.find({ surname: keyword });
//                     break;
//                     case "/phone"   :       cursor = collectionref.find({ phone: keyword });
//                     break;
//                 }
//                 cursor.toArray(function(err, docs) {
//                     if(err) console.log(err);
//                     var contacts = "";
//                     for (var i = 0; i < docs.length; i++) {
//                         contacts += docs[i].name + " " + docs[i].surname + ", " + docs[i].phone + "<br>";
//                     }
//                     res.writeHead(200, { "Content-Type": "text/html" });
//                     res.write(contacts);
//                     db.close();
//                 });
//             }
//         });
//     });
// });